use smart_house::device::{DeviceInfo, SmartSocket, SmartThermometer};
use smart_house::Result;
use smart_house::SmartHouse;

fn main() -> Result<()> {
    let room_name_1 = "гостиная";
    let room_name_2 = "спальня";

    let device_name_1 = "обычная";
    let device_name_2 = "евро";
    let device_name_3 = "комнатный";
    let device_name_4 = "наружный";

    // Инициализация устройств
    let socket_1 = SmartSocket {
        name: device_name_1.into(),
        switch: true,
        voltage: 220,
        amperage: 10,
    };
    let socket_2 = SmartSocket {
        name: device_name_2.into(),
        switch: false,
        voltage: 110,
        amperage: 0,
    };
    let therm = SmartThermometer {
        name: device_name_3.into(),
        temp: 30,
    };

    // Инициализация дома
    let house = SmartHouse::with_name("Умный дом")
        .add_device(room_name_1, device_name_1)
        .add_device(room_name_2, device_name_2)
        .add_device(room_name_2, device_name_3)
        // отсутствующий девайс
        .add_device(room_name_1, device_name_4);

    // Строим динамический отчет
    let info_provider_dyn: &[&dyn DeviceInfo] = &[&socket_1 as _, &socket_2 as _, &therm as _];
    let report_dyn = house.report_dyn(info_provider_dyn);

    println!("Info device provider: {:#?}", info_provider_dyn);
    println!("Report:\n{}", report_dyn?);

    Ok(())
}
