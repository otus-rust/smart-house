use smart_house::Result;
use smart_house::SmartHouse;

fn main() -> Result<()> {
    let house = SmartHouse::new();
    println!("House: {}", house.name);

    let rooms = house.rooms();
    println!("Rooms: {:?}", rooms?);

    Ok(())
}
