use smart_house::Result;
use smart_house::SmartHouse;

fn main() -> Result<()> {
    let room_1 = "гостиная";
    let room_2 = "спальня";

    let house = SmartHouse::with_name("Умный дом")
        .add_device(room_1, "обычная")
        .add_device(room_2, "евро")
        .add_device(room_2, "комнатный");

    let rooms = house.rooms();
    println!("Rooms: {:?}", rooms?);

    let devices = house.devices(room_1);
    println!("Devices ({room_1}): {:?}", devices?);

    let devices = house.devices(room_2);
    println!("Devices ({room_2}): {:?}", devices?);

    Ok(())
}
