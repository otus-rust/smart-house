use smart_house::device::provider::BorrowingDeviceInfoProvider;
use smart_house::device::SmartSocket;
use smart_house::device::SmartThermometer;
use smart_house::SmartHouse;

fn main() {
    let socket = SmartSocket {
        name: "евро".into(),
        switch: false,
        voltage: 110,
        amperage: 0,
    };
    let therm = SmartThermometer {
        name: "комнатный".into(),
        temp: 30,
    };

    // Инициализация дома
    let house = SmartHouse::with_name("Умный дом")
        .add_device("большая", "обычная")
        .add_device("маленькая", "евро")
        .add_device("маленькая", "комнатный");

    // Строим отчёт с использованием `BorrowingDeviceInfoProvider`.
    let info_provider = BorrowingDeviceInfoProvider {
        socket: &socket,
        therm: &therm,
    };

    let report = house.report(&info_provider);

    // Выводим отчёты на экран:
    println!("Report:\n{report}");
}
