use smart_house::device::provider::OwningDeviceInfoProvider;
use smart_house::device::SmartSocket;
use smart_house::SmartHouse;

fn main() {
    let socket = SmartSocket {
        name: "обычная".into(),
        switch: true,
        voltage: 220,
        amperage: 10,
    };

    // Инициализация дома
    let house = SmartHouse::with_name("Умный дом")
        .add_device("большая", "обычная")
        .add_device("маленькая", "евро")
        .add_device("маленькая", "комнатный");

    // Строим отчёт с использованием `OwningDeviceInfoProvider`.
    let info_provider = OwningDeviceInfoProvider { socket };

    let report = house.report(&info_provider);

    // Выводим отчёты на экран:
    println!("Report:\n{report}");
}
