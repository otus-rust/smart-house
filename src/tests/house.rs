use crate::device::provider::BorrowingDeviceInfoProvider;
use crate::device::provider::OwningDeviceInfoProvider;
use crate::device::SmartThermometer;
use crate::device::{DeviceInfo, SmartSocket};
use crate::{SmartHouse, SmartRoom};
use std::assert_matches::assert_matches;

fn create_smart_home() -> SmartHouse {
    SmartHouse::with_name("test")
        .add_device("test1", "test2")
        .add_device("test3", "test4")
        .add_device("test5", "test6")
        .add_device("test5", "test7")
}

#[test]
fn empty() {
    let house = SmartHouse::new();

    assert_eq!(house.name, "");
    assert_eq!(house.rooms.len(), 0);
}

#[test]
fn with_name() {
    let house = SmartHouse::with_name("test");

    assert_eq!(house.name, "test");
    assert_eq!(house.rooms.len(), 0);
}

#[test]
fn with_devices() {
    let house = create_smart_home();

    assert_eq!(house.name, "test");
    assert_eq!(house.rooms.len(), 3);
}

#[test]
fn add_rooms() {
    let house = SmartHouse::with_name("test")
        .add_room("test1")
        .add_room("test2")
        .add_room("test3");

    assert_eq!(house.rooms.len(), 3);
}

#[test]
fn remove_rooms() {
    let house = create_smart_home();

    assert_eq!(house.rooms.len(), 3);

    let house_removed = house
        .remove_room("test1")
        .remove_room("test3")
        .remove_room("test5");

    assert_eq!(house_removed.rooms.len(), 0);
}

#[test]
fn rooms() {
    let house = create_smart_home();

    assert_matches!(house.rooms(), Ok(rooms) if rooms.len() == 3);
}

#[test]
fn devices() {
    let house = create_smart_home();

    assert_matches!(house.devices("test1"), Ok(devices) if devices.len() == 1);
    assert_matches!(house.devices("test3"), Ok(devices) if devices.len() == 1);
    assert_matches!(house.devices("test5"), Ok(devices) if devices.len() == 2);
}

#[test]
fn devices_with_absent_room() {
    let house = create_smart_home();

    assert_matches!(house.devices("test0"), Err(_));
}

#[test]
#[allow(clippy::unwrap_used)]
fn lookup() {
    let house = create_smart_home();

    assert_eq!(
        house.lookup("test2").unwrap(),
        &SmartRoom::with_name("test1")
    );
    assert_eq!(
        house.lookup("test4").unwrap(),
        &SmartRoom::with_name("test3")
    );
    assert_eq!(
        house.lookup("test6").unwrap(),
        &SmartRoom::with_name("test5")
    );
    assert_eq!(
        house.lookup("test7").unwrap(),
        &SmartRoom::with_name("test5")
    );
}

#[test]
fn report_with_owning() {
    let socket = SmartSocket {
        name: "test2".to_string(),
        switch: true,
        voltage: 220,
        amperage: 10,
    };
    let info_provider = OwningDeviceInfoProvider { socket };

    let house = create_smart_home();

    assert!(!house.report(&info_provider).is_empty());
}

#[test]
fn report_with_borrowing() {
    let socket = SmartSocket {
        name: "test2".to_string(),
        switch: false,
        voltage: 110,
        amperage: 0,
    };
    let therm = SmartThermometer {
        name: "test4".to_string(),
        temp: 30,
    };
    let info_provider = BorrowingDeviceInfoProvider {
        socket: &socket,
        therm: &therm,
    };

    let house = create_smart_home();

    assert!(!house.report(&info_provider).is_empty());
}

#[test]
fn report_with_absent_device() {
    let socket = SmartSocket {
        name: "test0".to_string(),
        switch: true,
        voltage: 220,
        amperage: 10,
    };
    let info_provider = OwningDeviceInfoProvider { socket };

    let house = create_smart_home();

    let report = house.report(&info_provider);

    assert!(report.contains("не найдено!"));
}

#[test]
fn report_dyn() {
    let socket_1 = SmartSocket {
        name: "test2".into(),
        switch: false,
        voltage: 110,
        amperage: 0,
    };
    let socket_2 = SmartSocket {
        name: "test4".into(),
        switch: false,
        voltage: 110,
        amperage: 0,
    };
    let therm_1 = SmartThermometer {
        name: "test6".into(),
        temp: 30,
    };
    let therm_2 = SmartThermometer {
        name: "test7".into(),
        temp: 30,
    };

    let info_provider_dyn: &[&dyn DeviceInfo] = &[&socket_1, &socket_2, &therm_1, &therm_2];

    let house = create_smart_home();

    assert_matches!(house.report_dyn(info_provider_dyn), Ok(s) if !s.is_empty());
}

#[test]
fn report_dyn_with_absent_device() {
    let socket_1 = SmartSocket {
        name: "test2".into(),
        switch: false,
        voltage: 110,
        amperage: 0,
    };
    let socket_2 = SmartSocket {
        name: "test4".into(),
        switch: false,
        voltage: 110,
        amperage: 0,
    };
    let therm_1 = SmartThermometer {
        name: "test6".into(),
        temp: 30,
    };

    let info_provider_dyn: &[&dyn DeviceInfo] = &[&socket_1, &socket_2, &therm_1];

    let house = create_smart_home();

    assert_matches!(house.report_dyn(info_provider_dyn), Err(_));
}
