use std::collections::hash_map::RandomState;
use std::hash::BuildHasher;

use crate::SmartRoom;

#[test]
fn empty() {
    let room = SmartRoom::new();

    assert_eq!(room.name, "");
    assert_eq!(room.devices.len(), 0);
}

#[test]
fn with_name() {
    let room = SmartRoom::with_name("test");

    assert_eq!(room.name, "test");
    assert_eq!(room.devices.len(), 0);
}

#[test]
fn add_devices() {
    let room = SmartRoom::with_name("test")
        .add_device("test1")
        .add_device("test2")
        .add_device("test3");

    assert_eq!(room.name, "test");
    assert_eq!(room.devices.len(), 3);
}

#[test]
fn remove_devices() {
    let room = SmartRoom::with_name("test")
        .add_device("test1")
        .add_device("test2")
        .add_device("test3");
    assert_eq!(room.devices.len(), 3);

    let room_removed = room
        .remove_device("test1")
        .remove_device("test2")
        .remove_device("test3");

    assert_eq!(room_removed.devices.len(), 0);
}

#[test]
fn eq_and_hash() {
    let room1 = SmartRoom::with_name("test").add_device("test1");
    let room2 = SmartRoom::with_name("test")
        .add_device("test2")
        .add_device("test3");

    assert!(room1.eq(&room2));

    let hasher = RandomState::new();
    let hash1 = hasher.hash_one(room1);
    let hash2 = hasher.hash_one(room2);

    assert_eq!(dbg!(hash1), dbg!(hash2));
}

#[test]
fn ne() {
    let room1 = SmartRoom::with_name("test1");
    let room2 = SmartRoom::with_name("test2");

    assert!(room1.ne(&room2));
}
