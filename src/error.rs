use std::fmt::Debug;

use thiserror::Error;

#[derive(Debug, Error)]
pub enum Error {
    #[error("Ошибка: {0}")]
    RoomError(#[from] RoomError),
    #[error("Ошибка: {0}")]
    DeviceError(#[from] DeviceError),
    #[error("Ошибка: {0}")]
    Unknown(String),
}

#[derive(Debug, Error)]
pub enum RoomError {
    #[error("Комната '{0}' не найдена!")]
    NotFound(String),
    #[error("Комнаты не найдены!")]
    Empty,
}

#[derive(Debug, Error)]
pub enum DeviceError {
    #[error("Устройство '{0}' не найдено!")]
    NotFound(String),
    #[error("Устройства не найдены!")]
    Empty,
}
