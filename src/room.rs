use std::collections::HashSet;
use std::fmt::Debug;
use std::hash::{Hash, Hasher};

#[derive(Debug, Eq, Clone, Default)]
pub struct SmartRoom {
    pub(crate) name: String,
    pub(crate) devices: HashSet<String>,
}

impl PartialEq for SmartRoom {
    fn eq(&self, other: &Self) -> bool {
        self.name.eq(other.name.as_str())
    }
}

impl Hash for SmartRoom {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.name.hash(state);
    }
}

impl SmartRoom {
    #[must_use]
    pub fn new() -> Self {
        Self::with_name("")
    }

    #[must_use]
    pub fn with_name(name: &str) -> Self {
        Self {
            name: name.into(),
            devices: HashSet::default(),
        }
    }

    #[must_use]
    pub fn add_device(mut self, device: &str) -> Self {
        let _ = self.devices.insert(device.into());
        self
    }

    #[must_use]
    pub fn remove_device(mut self, device: &str) -> Self {
        let _ = self.devices.remove(device);
        self
    }
}

#[cfg(debug_assertions)]
#[allow(clippy::dbg_macro)]
impl Drop for SmartRoom {
    #[inline]
    fn drop(&mut self) {
        log::debug!("Dropping: {:?}", self);
    }
}
