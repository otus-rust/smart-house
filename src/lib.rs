#![feature(build_hasher_simple_hash_one)]
#![feature(assert_matches)]
#![deny(
    explicit_outlives_requirements,
    let_underscore_drop,
    macro_use_extern_crate,
    missing_debug_implementations,
    trivial_casts,
    trivial_numeric_casts,
    unreachable_pub,
    unsafe_code,
    unused_qualifications,
    unused_results,
    unused_variables,
    variant_size_differences,
    clippy::complexity,
    clippy::nursery,
    clippy::pedantic,
    clippy::perf,
    clippy::style,
    clippy::suspicious,
    clippy::clone_on_ref_ptr,
    clippy::create_dir,
    clippy::dbg_macro,
    clippy::default_numeric_fallback,
    clippy::else_if_without_else,
    clippy::empty_structs_with_brackets,
    clippy::expect_used,
    clippy::get_unwrap,
    clippy::map_err_ignore,
    clippy::multiple_inherent_impl,
    clippy::panic_in_result_fn,
    clippy::rc_mutex,
    clippy::rest_pat_in_fully_bound_structs,
    clippy::same_name_method,
    clippy::self_named_module_files,
    clippy::shadow_reuse,
    clippy::shadow_same,
    clippy::shadow_unrelated,
    clippy::unseparated_literal_suffix,
    clippy::string_to_string,
    clippy::todo,
    clippy::unimplemented,
    clippy::unreachable,
    clippy::unwrap_in_result,
    clippy::unwrap_used,
    clippy::use_debug,
    clippy::verbose_file_reads,
    clippy::wildcard_enum_match_arm,
    clippy::panic
)]
#![allow(missing_docs, clippy::pub_use, clippy::module_name_repetitions)]

use std::io::Write;
use std::result;

use chrono::Local;
use env_logger::{Env, WriteStyle};

use crate::error::{DeviceError, RoomError};

pub use self::error::Error;
pub use self::house::SmartHouse;
pub use self::room::SmartRoom;

pub mod device;
pub mod error;
pub mod house;
pub mod room;

#[cfg(test)]
mod tests;

pub type Result<T> = result::Result<T, Error>;
pub type RoomResult<T> = result::Result<T, RoomError>;
pub type DeviceResult<T> = result::Result<T, DeviceError>;

// Логгер
pub fn log_init() {
    #[cfg(debug_assertions)]
    env_logger::Builder::from_env(Env::default().default_filter_or("debug"))
        .write_style(WriteStyle::Auto)
        .format(|fmt, rec| {
            writeln!(
                fmt,
                "[{} {:5} {}:{}] {}",
                Local::now().format("%FT%X"),
                fmt.default_level_style(rec.level()).value(rec.level()),
                rec.file().unwrap_or("-"),
                rec.line().unwrap_or(0),
                rec.args()
            )
        })
        .init();
    #[cfg(not(debug_assertions))]
    env_logger::Builder::from_env(Env::default().default_filter_or("info"))
        .write_style(WriteStyle::Auto)
        .format(|fmt, rec| {
            writeln!(
                fmt,
                "[{} {:5} {}] {}",
                Local::now().format("%FT%X"),
                fmt.default_level_style(rec.level()).value(rec.level()),
                rec.target(),
                rec.args()
            )
        })
        .init();
}
