extern crate core;

use log::{error, info, warn};
use smart_house::device::provider::BorrowingDeviceInfoProvider;
use smart_house::device::provider::OwningDeviceInfoProvider;
use smart_house::device::SmartThermometer;
use smart_house::device::{DeviceInfo, SmartSocket};
use smart_house::{log_init, Result, SmartHouse};

fn main() {
    log_init();

    match run() {
        Ok(_) => (),
        Err(e) => error!("{}", e),
    }
    warn!("Exiting...");
}

fn run() -> Result<()> {
    // Инициализация
    let room_name_1 = "гостиная";
    let room_name_2 = "спальня";

    let device_name_1 = "обычная";
    let device_name_2 = "евро";
    let device_name_3 = "комнатный";
    let device_name_4 = "наружный";

    // Инициализация устройств
    let socket_1 = SmartSocket {
        name: device_name_1.into(),
        switch: true,
        voltage: 220,
        amperage: 10,
    };
    let socket_2 = SmartSocket {
        name: device_name_2.into(),
        switch: false,
        voltage: 110,
        amperage: 0,
    };
    let therm = SmartThermometer {
        name: device_name_3.into(),
        temp: 30,
    };

    // Инициализация дома
    let house = SmartHouse::with_name("Умный дом")
        .add_device(room_name_1, device_name_1)
        .add_device(room_name_2, device_name_2)
        .add_device(room_name_2, device_name_3);

    // Проверяем дом
    info!("Rooms: {:?}", house.rooms()?);
    info!("Devices ({room_name_1}): {:?}", house.devices(room_name_1)?);
    info!("Devices ({room_name_2}): {:?}", house.devices(room_name_2)?);

    // Строим динамический отчет
    let info_provider_dyn: &[&dyn DeviceInfo] = &[&socket_1 as _, &socket_2 as _, &therm as _];
    let report_dyn = house.report_dyn(info_provider_dyn);

    // Строим динамический отчет c отсутствующим девайсом -> ошибка
    let house = house.add_device(room_name_1, device_name_4);
    let report_dyn_err = house.report_dyn(info_provider_dyn);

    // Строим отчёт с использованием `OwningDeviceInfoProvider`.
    let info_provider_1 = OwningDeviceInfoProvider { socket: socket_1 };

    let report_1 = house.report(&info_provider_1);

    // Строим отчёт с использованием `BorrowingDeviceInfoProvider`.
    let info_provider_2 = BorrowingDeviceInfoProvider {
        socket: &socket_2,
        therm: &therm,
    };

    let report_2 = house.report(&info_provider_2);

    // Выводим отчёты на экран:
    info!("Report #1:\n{report_1}");
    info!("Report #2:\n{report_2}");
    info!("Report #3:\n{}", report_dyn?);

    // После того как добавилось неизвестное устройство
    info!("Rooms: {:?}", house.rooms()?);
    info!("Devices ({room_name_1}): {:?}", house.devices(room_name_1)?);
    info!("Devices ({room_name_2}): {:?}", house.devices(room_name_2)?);

    // Выводится ошибка вместо отчета
    info!("Report #4:\n{}", report_dyn_err?);

    Ok(())
}
