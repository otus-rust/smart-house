use crate::device::DeviceInfo;
use std::fmt::{Display, Formatter, Result};

#[derive(Debug, Clone)]
pub struct SmartThermometer {
    pub name: String,
    pub temp: i32,
}

impl DeviceInfo for SmartThermometer {
    fn name(&self) -> String {
        self.name.clone()
    }

    fn info(&self) -> String {
        self.to_string()
    }
}

impl Display for SmartThermometer {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        writeln!(f, "  └ Устройство: термометр")?;
        writeln!(f, "    └ Название: {}", self.name)?;
        writeln!(f, "    └ Температура: {}", self.temp)
    }
}

#[cfg(debug_assertions)]
#[allow(clippy::dbg_macro)]
impl Drop for SmartThermometer {
    #[inline]
    fn drop(&mut self) {
        log::debug!("Dropping: {:?}", self);
    }
}
