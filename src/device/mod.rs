use std::fmt::{Debug, Display};

// Пользовательские устройства:
pub use self::socket::SmartSocket;
pub use self::thermometer::SmartThermometer;

pub mod provider;
pub mod socket;
pub mod thermometer;

pub trait DeviceInfo: Debug + Display {
    fn name(&self) -> String;

    fn info(&self) -> String;
}
