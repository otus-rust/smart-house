use std::collections::HashMap;

use crate::device::provider::DeviceInfoProvider;
use crate::device::DeviceInfo;
use crate::device::SmartSocket;
use crate::device::SmartThermometer;

#[derive(Debug)]
pub struct BorrowingDeviceInfoProvider<'a, 'b> {
    pub socket: &'a SmartSocket,
    pub therm: &'b SmartThermometer,
}

impl DeviceInfoProvider for BorrowingDeviceInfoProvider<'_, '_> {
    fn info(&self) -> HashMap<String, String> {
        HashMap::from([
            (self.socket.name.clone(), self.socket.info()),
            (self.therm.name.clone(), self.therm.info()),
        ])
    }
}
