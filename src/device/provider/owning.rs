use std::collections::HashMap;

use crate::device::provider::DeviceInfoProvider;
use crate::device::DeviceInfo;
use crate::device::SmartSocket;

#[derive(Debug)]
pub struct OwningDeviceInfoProvider {
    pub socket: SmartSocket,
}

impl DeviceInfoProvider for OwningDeviceInfoProvider {
    fn info(&self) -> HashMap<String, String> {
        HashMap::from([(self.socket.name.clone(), self.socket.info())])
    }
}
