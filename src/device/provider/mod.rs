use std::collections::HashMap;

// Пользовательские поставщики информации об устройствах.
// Могут как хранить устройства, так и заимствовать.
pub use self::borrowing::BorrowingDeviceInfoProvider;
pub use self::owning::OwningDeviceInfoProvider;

pub mod borrowing;
pub mod owning;

pub trait DeviceInfoProvider {
    fn info(&self) -> HashMap<String, String>;
}
