use std::fmt::{Display, Formatter, Result};

use crate::device::DeviceInfo;

#[derive(Debug, Clone)]
pub struct SmartSocket {
    pub name: String,
    pub switch: bool,
    pub voltage: u32,
    pub amperage: u32,
}

impl DeviceInfo for SmartSocket {
    fn name(&self) -> String {
        self.name.clone()
    }

    fn info(&self) -> String {
        self.to_string()
    }
}

impl Display for SmartSocket {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        writeln!(f, "  └ Устройство: розетка")?;
        writeln!(f, "    └ Название: {}", self.name)?;
        writeln!(
            f,
            "    └ Состояние: {}",
            if self.switch { "вкл" } else { "выкл" }
        )?;
        writeln!(f, "    └ Напряжение: {}", self.voltage)?;
        writeln!(
            f,
            "    └ Сила тока: {}",
            if self.switch { self.amperage } else { 0 }
        )?;
        writeln!(
            f,
            "    └ Мощность: {}",
            if self.switch {
                self.voltage * self.amperage
            } else {
                0
            }
        )
    }
}

#[cfg(debug_assertions)]
#[allow(clippy::dbg_macro)]
impl Drop for SmartSocket {
    #[inline]
    fn drop(&mut self) {
        log::debug!("Dropping: {:?}", self);
    }
}
