use std::collections::HashSet;
use std::fmt::Debug;

use crate::device::provider::DeviceInfoProvider;
use crate::device::DeviceInfo;
use crate::error::{DeviceError, RoomError};
use crate::{Error, Result, SmartRoom};

#[derive(Debug, Default)]
pub struct SmartHouse {
    pub name: String,
    pub rooms: HashSet<SmartRoom>,
}

impl SmartHouse {
    #[must_use]
    pub fn new() -> Self {
        Self::with_name("")
    }

    #[must_use]
    pub fn with_name(name: &str) -> Self {
        Self {
            name: name.into(),
            rooms: HashSet::default(),
        }
    }

    #[must_use]
    pub fn add_device(mut self, room_name: &str, device_name: &str) -> Self {
        let room_opt = self.rooms.iter().find(|r| r.name.eq(room_name));
        match room_opt {
            None => {
                let _ = self
                    .rooms
                    .insert(SmartRoom::with_name(room_name).add_device(device_name));
            }
            Some(room) => {
                let _droppable = self.rooms.replace(room.clone().add_device(device_name));
            }
        }
        self
    }

    #[must_use]
    pub fn add_room(mut self, room_name: &str) -> Self {
        let room_opt = self.rooms.iter().find(|room| room.name.eq(room_name));
        match room_opt {
            None => {
                let _ = self.rooms.insert(SmartRoom::with_name(room_name));
            }
            Some(_) => (),
        }
        self
    }

    #[must_use]
    pub fn remove_room(mut self, room_name: &str) -> Self {
        let _ = self.rooms.remove(&SmartRoom::with_name(room_name));
        self
    }

    /// # Errors
    /// returns error if rooms not found
    pub fn rooms(&self) -> Result<Vec<String>> {
        match self.rooms.len() {
            0 => Err(RoomError::Empty)?,
            1..=usize::MAX => Ok(self.rooms.iter().map(|room| room.name.clone()).collect()),
            _ => Err(Error::Unknown("Что то пошло не так!".into())),
        }
    }

    /// Returns array of devices in the given room.
    /// # Errors
    /// returns error if room mot found
    pub fn devices(&self, room_name: &str) -> Result<Vec<String>> {
        self.rooms
            .iter()
            .find(|room| room.name.eq(room_name))
            .map(|room| room.devices.iter().map(String::clone).collect())
            .map_or_else(|| Err(RoomError::NotFound(room_name.into()))?, Ok)
    }

    /// Returns devices info.
    pub fn report(&self, provider: &impl DeviceInfoProvider) -> String {
        let mut buf = format!("Дом: {}\n", self.name);
        for (device, info) in provider.info() {
            match self.lookup(device.as_str()) {
                None => buf.push_str(format!("Устройство: {device} - не найдено!").as_str()),
                Some(room) => {
                    buf.push_str(format!("└ Комната: {}\n", room.name).as_str());
                    buf.push_str(info.as_str());
                }
            }
        }
        buf
    }

    /// # Errors
    /// returns Err if device not found in devices info provider
    pub fn report_dyn(&self, provider: &[&dyn DeviceInfo]) -> Result<String> {
        let mut buf = format!("Дом: {}\n", self.name);
        for room in &self.rooms {
            buf.push_str(format!("└ Комната: {}\n", room.name).as_str());
            for device in &room.devices {
                let info = Self::lookup_dyn(device.as_str(), provider)?;
                buf.push_str(info.info().as_str());
            }
        }
        Ok(buf)
    }

    pub(crate) fn lookup(&self, device_name: &str) -> Option<&SmartRoom> {
        self.rooms
            .iter()
            .find(|room| room.devices.contains(device_name))
    }

    pub(crate) fn lookup_dyn<'a>(
        device_name: &str,
        provider: &[&'a dyn DeviceInfo],
    ) -> Result<&'a (dyn DeviceInfo + 'a)> {
        provider
            .iter()
            .find(|device| device.name().eq(device_name))
            .map_or_else(|| Err(DeviceError::NotFound(device_name.into()))?, Ok)
            .copied()
    }
}

#[cfg(debug_assertions)]
#[allow(clippy::dbg_macro)]
impl Drop for SmartHouse {
    #[inline]
    fn drop(&mut self) {
        log::debug!("Dropping: {:?}", self);
    }
}
